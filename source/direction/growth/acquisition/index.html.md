---
layout: markdown_page
title: Product Vision - Acquisition
---

### Overview

The Acquisition Team at GitLab focuses on running experiments to increase the paid adoption of our platform at the point of signup, we strive to promote the value of GitLab and accelerate site visitors transition to happy valued users of our product. Acquisition sits at the beginning of the journey for individual users and organizations as they get their first introduction to the value proposition of GitLab. Our goal is to ensure that users understand the unique value that GitLab provides and thereby ensure that prospects seamlessly transition into healthy, valued users of GitLab. We will gain a deep understanding of the value that our users realize from the different Stages of GitLab, and ensure that value is susinctly communicated to our site visitors in order to efficently transition them from prospects to active, valued users.

**Acquisition Team**

Product Manager: [Jensen Stava](https://about.gitlab.com/company/team/#jstava) | Engineering Manager: [Phil Calder (Interim)](https://about.gitlab.com/company/team/#pcalder) | UX Manager: [Jacki Bauer](https://about.gitlab.com/company/team/#jackib) | Product Designer: [Kevin Comoli](https://about.gitlab.com/company/team/#kcomoli) | Full Stack Engineer: [Alex Buijs](https://about.gitlab.com/company/team/#alexbuijs) | Full Stack Engineer: [Nicolas Dular](https://about.gitlab.com/company/team/#nicolasdular)

**Acquisition Team Mission**

*   To promote the value of GitLab and accelerate site visitors transition to happy valued users of our product.

**Acquisition KPI**


*   Direct signup ARR growth rate
*   Definition:  (Direct signup ARR in the current period - direct signup revenue in the prior period)/direct signup revenue in the prior period

_Supporting performance indicators:_

1. Time to sign up
   - Definition: Time of account signup completion - Time of first visit
2. Percent of paid conversion 
   - Definition: Total number of unique site visitors / Total number of  paid signups
3. Percent of conversion 
   - Definition: Total number of unique site visitors / Total number of signups
4. Free to paid conversion
   - Definition: Free to paid conversion rate (free user upgrades / free user signups in period)
5. Time to upgrade
   - Time of account signup completion - Time of upgrade to paid package

### Problems to solve

Do you have issues... We’d love to hear about them, how can we help GitLab contributors find that ah-Ha! Moment. 

Here are few problems we are trying to solve: 


*   **Clarity** - Whose problems do you solve?
    *   Your product does A LOT, the feature set is expansive, but I can't tell if it will work for me.
*   **Call to Action** - I want to signup! What is the best way to do that? 
    *   I can't tell which offering of GitLab I should signup for? 
    *   What package should I signup for?
*   **Usability** - Signing up for a paid package is a cumbersome process...
    *   As a dev-ops lead, how do I buy this product for my team?
    *   How do I signup for a paid package from my self hosted instance?


### Our approach

Acquisiton runs a standard growth process. We gather feedback, analyze the information, ideate then create experiments and test. As you can imagine the amount of ideas we can come up with is enormous so we use the [ICE framework](https://blog.growthhackers.com/the-practical-advantage-of-the-ice-score-as-a-test-prioritization-framework-cdd5f0808d64) (impact, confidence, effort) to ensure we are focusing on the experiments that will provide the most value. 


### Maturity

The growth team at GitLab is new and we are iterating along the way. As of August 2019 we have just formed the expansion group and are planning to become a more mature, organized and well oiled machine by January 2020. 


### Helpful Links

[GitLab Growth project](https://gitlab.com/gitlab-org/growth)

[KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)

Reports & Dashboards